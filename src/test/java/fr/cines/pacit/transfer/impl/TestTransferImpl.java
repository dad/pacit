/*
 * Copyright CINES, 2022
 * Ce logiciel est r�gi par la licence CeCILL-C soumise au
 * droit fran�ais et respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions de la licence CeCILL-C
 * telle que diffus�e par le CEA, le CNRS et l'INRIA sur le site "http://www.cecill.info". En
 * contrepartie de l'accessibilit� au code source et des droits de copie, de modification et de
 * redistribution accord�s par cette licence, il n'est offert aux utilisateurs qu'une garantie
 * limit�e. Pour les m�mes raisons, seule une responsabilit� restreinte p�se sur l'auteur du
 * programme, le titulaire des droits patrimoniaux et les conc�dants successifs. A cet �gard
 * l'attention de l'utilisateur est attir�e sur les risques associ�s au chargement, � l'utilisation,
 * � la modification et/ou au d�veloppement et � la reproduction du logiciel par l'utilisateur �tant
 * donn� sa sp�cificit� de logiciel libre, qui peut le rendre complexe � manipuler et qui le r�serve
 * donc � des d�veloppeurs et des professionnels avertis poss�dant des connaissances informatiques
 * approfondies. Les utilisateurs sont donc invit�s � charger et tester l'ad�quation du logiciel �
 * leurs besoins dans des conditions permettant d'assurer la s�curit� de leurs syst�mes et ou de
 * leurs donn�es et, plus g�n�ralement, � l'utiliser et l'exploiter dans les m�mes conditions de
 * s�curit�. Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez pris
 * connaissance de la licence CeCILL-C, et que vous en avez accept� les termes. 
*/
package fr.cines.pacit.transfer.impl;

import static org.junit.Assert.*;

import java.io.File;
import java.nio.file.Files;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import fr.cines.pacit.tranfer.Transfer;

/**
 * @author Raphael Ella <ella@cines.fr>
 *
 */
public class TestTransferImpl {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void transferTest() {
		
		File file = new File("C:/Users/ella/Desktop/testTransfer/vitam-data-test-master");
		
		TransferImpl testTranfer = new TransferImpl();
		 System.out.println(testTranfer.listFile(file));
	}

}
