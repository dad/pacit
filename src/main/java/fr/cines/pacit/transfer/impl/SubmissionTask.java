/*
 * Copyright CINES, 2022
 * Ce logiciel est r�gi par la licence CeCILL-C soumise au
 * droit fran�ais et respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions de la licence CeCILL-C
 * telle que diffus�e par le CEA, le CNRS et l'INRIA sur le site "http://www.cecill.info". En
 * contrepartie de l'accessibilit� au code source et des droits de copie, de modification et de
 * redistribution accord�s par cette licence, il n'est offert aux utilisateurs qu'une garantie
 * limit�e. Pour les m�mes raisons, seule une responsabilit� restreinte p�se sur l'auteur du
 * programme, le titulaire des droits patrimoniaux et les conc�dants successifs. A cet �gard
 * l'attention de l'utilisateur est attir�e sur les risques associ�s au chargement, � l'utilisation,
 * � la modification et/ou au d�veloppement et � la reproduction du logiciel par l'utilisateur �tant
 * donn� sa sp�cificit� de logiciel libre, qui peut le rendre complexe � manipuler et qui le r�serve
 * donc � des d�veloppeurs et des professionnels avertis poss�dant des connaissances informatiques
 * approfondies. Les utilisateurs sont donc invit�s � charger et tester l'ad�quation du logiciel �
 * leurs besoins dans des conditions permettant d'assurer la s�curit� de leurs syst�mes et ou de
 * leurs donn�es et, plus g�n�ralement, � l'utiliser et l'exploiter dans les m�mes conditions de
 * s�curit�. Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez pris
 * connaissance de la licence CeCILL-C, et que vous en avez accept� les termes. 
 */
package fr.cines.pacit.transfer.impl;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

import fr.cines.pacit.parse.ParseXml;
import fr.cines.pacit.tranfer.ArchiveService;

/**
 * @author Raphael Ella <ella@cines.fr>
 *
 */
public class SubmissionTask implements Runnable{

    private ArchiveService archiveService;
    private Path path;
    private Path pathAtrExpected;
    private String atrRuLes= "/pacit/src/main/resources/rules.xml";
    Path atrRule = Paths.get(atrRuLes);
    private static Map<String, String> mapRules = new HashMap<String, String>();
    private static Map<String, String> mapResult = new HashMap<String, String>();
    private static Map<String, String> mapExpected = new HashMap<String, String>();

    private ParseXml parser = new ParseXml();


    public SubmissionTask(Path path, ArchiveService archiveSerice, Path pathAtrExpected) {
        this.archiveService = archiveSerice;
        this.path = path;
        this.pathAtrExpected = pathAtrExpected;
    }

    @Override
    public void run() {

        try {
            String idRequest = archiveService.put(this.path);
            while(!isStatutTerminal(idRequest)) {
                Thread.sleep(5000);
            }
            Thread.sleep(5000);
            String result = archiveService.retreiveResult(idRequest);
            if (result != null) {
                mapResult =	parser.buildMap(result);
                mapExpected = parser.buildMap(pathAtrExpected);
                mapRules= parser.buildMap(atrRule);

                ParseXml pacit = new ParseXml();
                boolean comparaison = pacit.compare(mapExpected, mapResult, mapRules);

                System.out.println(comparaison);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private boolean isStatutTerminal(String idRequest) throws JsonMappingException, JsonProcessingException, URISyntaxException, IOException, InterruptedException {
        String status = archiveService.checkRequest(idRequest);
        System.out.println("check request status request: " + idRequest+ " Thread "+ Thread.currentThread().getName());
        if(status.equals("COMPLETED"))
            return true;
        else {
            return false;
        }
    }

}
