/*
 * Copyright CINES, 2022
 * Ce logiciel est r�gi par la licence CeCILL-C soumise au
 * droit fran�ais et respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions de la licence CeCILL-C
 * telle que diffus�e par le CEA, le CNRS et l'INRIA sur le site "http://www.cecill.info". En
 * contrepartie de l'accessibilit� au code source et des droits de copie, de modification et de
 * redistribution accord�s par cette licence, il n'est offert aux utilisateurs qu'une garantie
 * limit�e. Pour les m�mes raisons, seule une responsabilit� restreinte p�se sur l'auteur du
 * programme, le titulaire des droits patrimoniaux et les conc�dants successifs. A cet �gard
 * l'attention de l'utilisateur est attir�e sur les risques associ�s au chargement, � l'utilisation,
 * � la modification et/ou au d�veloppement et � la reproduction du logiciel par l'utilisateur �tant
 * donn� sa sp�cificit� de logiciel libre, qui peut le rendre complexe � manipuler et qui le r�serve
 * donc � des d�veloppeurs et des professionnels avertis poss�dant des connaissances informatiques
 * approfondies. Les utilisateurs sont donc invit�s � charger et tester l'ad�quation du logiciel �
 * leurs besoins dans des conditions permettant d'assurer la s�curit� de leurs syst�mes et ou de
 * leurs donn�es et, plus g�n�ralement, � l'utiliser et l'exploiter dans les m�mes conditions de
 * s�curit�. Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez pris
 * connaissance de la licence CeCILL-C, et que vous en avez accept� les termes. 
*/

package fr.cines.pacit.transfer.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class HttpTransferImpl {

	private Properties prop;
	private String localResult;
	public String rules;

	private List<Path> paths = new ArrayList<Path>();

	public Map<String, String> mapZipFile = new HashMap();
	private Map<String, String> mapRulesFile = new HashMap();


	String url ;
	String localPath ;
	String fileName = "//vitam-data-test.zip";
	String compessedFile = localPath + "vitam-data-test.zip";

	public void downloadFile(String url, String localPath) throws IOException  
	{
		URL ulr = new URL(url);
		HttpURLConnection connection = (HttpURLConnection) ulr.openConnection();
		connection.setRequestProperty("accept", "*/*");

		ReadableByteChannel readableFileChannel = Channels.newChannel(connection.getInputStream());
		FileOutputStream fos = new FileOutputStream(compessedFile);
		fos.getChannel().transferFrom(readableFileChannel, 0, Long.MAX_VALUE);
		fos.close();
		
		// Unzip
		byte[] buffer = new byte[1024];
        ZipInputStream zis = new ZipInputStream(new FileInputStream(compessedFile));
        ZipEntry zipEntry = zis.getNextEntry();
        while (zipEntry != null) {
        	File newFile = givePathFile(new File(localPath), zipEntry);
            if (zipEntry.isDirectory()) {
                if (!newFile.isDirectory() && !newFile.mkdirs()) {
                    throw new IOException("Failed to create directory " + newFile);
                }
            } else {
                // fix for Windows-created archives
                File parent = newFile.getParentFile();
                if (!parent.isDirectory() && !parent.mkdirs()) {
                    throw new IOException("Failed to create directory " + parent);
                }
                
                // write file content
                FileOutputStream entryOutputStream = new FileOutputStream(newFile);
                int len;
                while ((len = zis.read(buffer)) > 0) {
                	entryOutputStream.write(buffer, 0, len);
                }
                entryOutputStream.close();
            }
        zipEntry = zis.getNextEntry();
        }
        zis.closeEntry();
        zis.close();
	}
	
	public static File givePathFile(File destinationDir, ZipEntry zipEntry) throws IOException {
	    File destFile = new File(destinationDir, zipEntry.getName());

	    String destDirPath = destinationDir.getCanonicalPath();
	    String destFilePath = destFile.getCanonicalPath();

	    if (!destFilePath.startsWith(destDirPath + File.separator)) {
	        throw new IOException("Entry is outside of the target dir: " + zipEntry.getName());
	    }

	    return destFile;
	}

	public  void deletezip() {
		File fileZip = new File(compessedFile) ;
		try {
		if(fileZip.delete()) {
			System.out.println("fichier zip supprim�");
		}else {
			System.out.println("fichier zip n'a pas �t� supprim�");
		}
		}catch(Exception e) {
			e.printStackTrace();
			//System.out.println(e.getMessage());
		}
	}
	
	
	public  void getTestFiles() {

		HttpTransferImpl p = new HttpTransferImpl(url, localPath) ;
		try {
			p.downloadFile(url, localPath);
			p.deletezip();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println(e);
		}

		System.out.print("fin telechargement du fichier ..........");
		File file = new File(localPath);

		listFile(file);
	}

	public void listFile(File folder) {

		for (File currentFile : folder.listFiles()) {
			if(currentFile.isDirectory()) {
				this.listFile(currentFile);
			} else if (currentFile.toString().endsWith("zip")) 
			{
				String fileNameWithoutExtension;

				String name = currentFile.getName();
				fileNameWithoutExtension = name.replaceFirst("[.][^.]+$", "");

				mapZipFile.put(fileNameWithoutExtension,currentFile.getAbsolutePath());
				
			}else if (currentFile.toString().endsWith("xml") && currentFile.getParent().contains("RESULTATS")) {
				
				String fileNameWithoutExtension;

				String name = currentFile.getName();
				fileNameWithoutExtension = name.replaceFirst("[.][^.]+$", "");

				mapRulesFile.put(fileNameWithoutExtension,currentFile.getAbsolutePath());
			}
		}
	}

	public HttpTransferImpl(String url, String localPath) {
		this.url = url ;
		this.localPath = localPath ;
	}

	public HttpTransferImpl() {
		try {
			prop = this.loadProperties();
			url = prop.getProperty("url");
			localPath = prop.getProperty("localPath");
			localResult = prop.getProperty("localResult");
			rules = prop.getProperty("rules");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public Properties loadProperties() throws IOException {
		Properties prop = new Properties() ;
		String configFolder ="C:/Users/ella/git/pacit/src/main/resources/" ;
		InputStream is = new FileInputStream(configFolder + File.separator + "config.properties");
		prop.load(is);
		is.close();

		return prop ;
	}
	

	public Map<String, String> getMapZipFile() {
		return mapZipFile;
	}

	public void setMapZipFile(Map<String, String> mapZipFile) {
		this.mapZipFile = mapZipFile;
	}

	public Map<String, String> getMapRulesFile() {
		return mapRulesFile;
	}

	public void setMapRulesFile(Map<String, String> mapRulesFile) {
		this.mapRulesFile = mapRulesFile;
	}

}
