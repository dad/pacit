/*
 * Copyright CINES, 2022
 * Ce logiciel est r�gi par la licence CeCILL-C soumise au
 * droit fran�ais et respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions de la licence CeCILL-C
 * telle que diffus�e par le CEA, le CNRS et l'INRIA sur le site "http://www.cecill.info". En
 * contrepartie de l'accessibilit� au code source et des droits de copie, de modification et de
 * redistribution accord�s par cette licence, il n'est offert aux utilisateurs qu'une garantie
 * limit�e. Pour les m�mes raisons, seule une responsabilit� restreinte p�se sur l'auteur du
 * programme, le titulaire des droits patrimoniaux et les conc�dants successifs. A cet �gard
 * l'attention de l'utilisateur est attir�e sur les risques associ�s au chargement, � l'utilisation,
 * � la modification et/ou au d�veloppement et � la reproduction du logiciel par l'utilisateur �tant
 * donn� sa sp�cificit� de logiciel libre, qui peut le rendre complexe � manipuler et qui le r�serve
 * donc � des d�veloppeurs et des professionnels avertis poss�dant des connaissances informatiques
 * approfondies. Les utilisateurs sont donc invit�s � charger et tester l'ad�quation du logiciel �
 * leurs besoins dans des conditions permettant d'assurer la s�curit� de leurs syst�mes et ou de
 * leurs donn�es et, plus g�n�ralement, � l'utiliser et l'exploiter dans les m�mes conditions de
 * s�curit�. Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez pris
 * connaissance de la licence CeCILL-C, et que vous en avez accept� les termes. 
 */
package fr.cines.pacit.transfer.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.http.HttpResponse;
import java.nio.file.Path;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;


import fr.cines.pacit.tranfer.ArchiveService;
import fr.cines.vitam.proxy.VitamApiProxyRequest;
import fr.cines.vitam.proxy.request.RequesterInfo;

/**
 * @author Raphael Ella <ella@cines.fr>
 *
 */
public class VitamArchiveService implements ArchiveService {

	private static Logger LOGGER = LoggerFactory.getLogger(VitamArchiveService.class);

	String IdRequest ;

	@Override
	public String put(Path path) throws IOException, URISyntaxException, InterruptedException{
		//		RequesterInfo requestInfo = new RequesterInfo("0", "CE-000001", "CINES_WORKFLOW");
		RequesterInfo requestInfo = new RequesterInfo();
		requestInfo.setContextID("CINES_WORKFLOW");
		requestInfo.setContrat_acces("CE-000001");
		requestInfo.setTenant_id("0");
		
		//		requestInfo.setClientCert(null);
		System.out.println("Soumission de " + path.toString());
		Thread.sleep(500);
		InputStream is = new FileInputStream(path.toFile());
		HttpResponse<String> ExternalResponse = VitamApiProxyRequest.Ingest().ingestFile(requestInfo, is);
		String requestId = ExternalResponse.headers().firstValue("X-Request-Id").get();
		if(ExternalResponse.statusCode() == HttpStatus.SC_ACCEPTED) {
			LOGGER.info("Ingestion accepted - RequestId {}", requestId);
			System.out.println("Ingestion accepted - RequestId " + requestId);


		}
		return requestId;


	}

	@Override
	public String checkRequest(String idRequest) throws URISyntaxException, IOException, InterruptedException {

		RequesterInfo requestInfo = new RequesterInfo();


		HttpResponse<String> httpResponse = VitamApiProxyRequest.Admin().getArchiveStatus(requestInfo, idRequest);
		ObjectMapper mapper = new ObjectMapper();
		JsonNode jsonResponse = mapper.readTree(httpResponse.body());
		String status = jsonResponse.get("$results").get(0).get("globalStatus").asText();
		String state = jsonResponse.get("$results").get(0).get("globalState").asText();
		return state;

	}

	@Override
	public String retreiveResult(String idRequest) {
		RequesterInfo requestInfo = new RequesterInfo();
		requestInfo.setContextID("CINES_WORKFLOW");
        requestInfo.setContrat_acces("CE-000001");
        requestInfo.setTenant_id("0");
		HttpResponse<String> ingestExternalResponse;
		try {
		    System.out.println("retreive result : " + idRequest);
			ingestExternalResponse = VitamApiProxyRequest.Ingest().getATR(requestInfo, idRequest);
			int responseCode = ingestExternalResponse.statusCode();
			
			if(responseCode == 200) {
			    String atr = ingestExternalResponse.body();
                return atr ;
            }

		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.err.println("Erreur : Pas d'ATR");
		return null;
	}

}
