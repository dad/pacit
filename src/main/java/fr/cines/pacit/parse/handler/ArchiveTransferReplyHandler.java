/**
 * 
 */
package fr.cines.pacit.parse.handler;

import java.util.HashMap;
import java.util.Map;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * @author ella
 *
 */
public class ArchiveTransferReplyHandler extends DefaultHandler {
	
	private Map<String, String> map = new HashMap<String, String>();
	
	private String currentTag;
	
	//private Stack<String> stack = new Stack<>();
	
	
	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		// TODO Auto-generated method stub
		super.startElement(uri, localName, qName, attributes);
		currentTag = qName;
		map.put(currentTag, "");
		
		
	}
	
	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {
		String value = new String(ch, start, length);
		super.characters(ch, start, length);
		map.put(currentTag, value);

	}
	public Map<String, String> getMap() {
		return map;
	}
	public void setMap(Map<String, String> map) {
		this.map = map;
	}	

}
