/**
 * 
 */
package fr.cines.pacit.parse;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.nio.file.Path;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import fr.cines.pacit.parse.handler.ArchiveTransferReplyHandler;

/**
 * @author Raphael Ella <ella@cines.fr>
 *
 */
public class ParseXml {
	
	
	public Map<String,String> buildMap(Path filePath) throws SAXException, ParserConfigurationException {

		SAXParserFactory spf = SAXParserFactory.newInstance();
		spf.setNamespaceAware(false);
		SAXParser parser = spf.newSAXParser();

		XMLReader reader = parser.getXMLReader();
		ArchiveTransferReplyHandler handler = new ArchiveTransferReplyHandler();
		reader.setContentHandler(handler);

		InputStream fichier;
		try {
			fichier = new FileInputStream(filePath.toFile());
			InputSource is = new InputSource(fichier);

			reader.parse(is);

			return handler.getMap();

		} catch (FileNotFoundException e) {
			System.out.println("fichier " + filePath + " introuvable");
			System.exit(-1);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;	
	}
	
	public Map<String,String> buildMap(String stringCharacters) throws SAXException, ParserConfigurationException {

		SAXParserFactory spf = SAXParserFactory.newInstance();
		spf.setNamespaceAware(false);
		SAXParser parser = spf.newSAXParser();

		XMLReader reader = parser.getXMLReader();
		ArchiveTransferReplyHandler handler = new ArchiveTransferReplyHandler();
		reader.setContentHandler(handler);

		
		InputStream fichier;
		try {
			StringReader stringReader = new StringReader(stringCharacters);
			InputSource is = new InputSource(stringReader);

			reader.parse(is);

			return handler.getMap();

		} catch (FileNotFoundException e) {
			System.out.println("ATR est introuvable :[ ");
			System.exit(-1);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;	
	}
}
