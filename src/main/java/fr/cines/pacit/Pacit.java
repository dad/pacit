/*
 * Copyright CINES, 2022
 * Ce logiciel est r�gi par la licence CeCILL-C soumise au
 * droit fran�ais et respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions de la licence CeCILL-C
 * telle que diffus�e par le CEA, le CNRS et l'INRIA sur le site "http://www.cecill.info". En
 * contrepartie de l'accessibilit� au code source et des droits de copie, de modification et de
 * redistribution accord�s par cette licence, il n'est offert aux utilisateurs qu'une garantie
 * limit�e. Pour les m�mes raisons, seule une responsabilit� restreinte p�se sur l'auteur du
 * programme, le titulaire des droits patrimoniaux et les conc�dants successifs. A cet �gard
 * l'attention de l'utilisateur est attir�e sur les risques associ�s au chargement, � l'utilisation,
 * � la modification et/ou au d�veloppement et � la reproduction du logiciel par l'utilisateur �tant
 * donn� sa sp�cificit� de logiciel libre, qui peut le rendre complexe � manipuler et qui le r�serve
 * donc � des d�veloppeurs et des professionnels avertis poss�dant des connaissances informatiques
 * approfondies. Les utilisateurs sont donc invit�s � charger et tester l'ad�quation du logiciel �
 * leurs besoins dans des conditions permettant d'assurer la s�curit� de leurs syst�mes et ou de
 * leurs donn�es et, plus g�n�ralement, � l'utiliser et l'exploiter dans les m�mes conditions de
 * s�curit�. Le fait que vous puissiez acc�der � cet en-t�te signifie que vous avez pris
 * connaissance de la licence CeCILL-C, et que vous en avez accept� les termes. 
 */
package fr.cines.pacit;


import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import fr.cines.pacit.tranfer.ArchiveService;
import fr.cines.pacit.tranfer.ArchiveServiceFactory;
import fr.cines.pacit.transfer.impl.HttpTransferImpl;
import fr.cines.pacit.transfer.impl.SubmissionTask;
import fr.cines.pacit.transfer.impl.VitamArchiveService;

/**
 * @author Raphael Ella <ella@cines.fr>
 *
 */
public class Pacit {

	/**
	 * @param args
	 * @throws SAXException 
	 * @throws ParserConfigurationException 
	 */



	private static Map<String, String> mapZipFile = new HashMap<String, String>();
	private static Map<String, String> mapRulesFiles = new HashMap<String, String>();

	public static void main(String[] args) throws SAXException, ParserConfigurationException {


		HttpTransferImpl transfer = new HttpTransferImpl();
		transfer.getTestFiles();
		mapZipFile = transfer.getMapZipFile() ;
		mapRulesFiles = transfer.getMapRulesFile();

		ArchiveService archiveService = ArchiveServiceFactory.getInstance();

		List<SubmissionTask> submissionList = new ArrayList<SubmissionTask>();

		for (Map.Entry mapentry : mapZipFile.entrySet()) {
			String pathToExpectedResult = mapRulesFiles.get(mapentry.getKey());
			if(pathToExpectedResult != null) {
				Path path = Paths.get(mapentry.getValue().toString());
				SubmissionTask task = new SubmissionTask(path, archiveService, Paths.get(pathToExpectedResult));
				submissionList.add(task);
			}
		}

		ExecutorService executor = Executors.newFixedThreadPool(3);

		for(SubmissionTask s : submissionList){
			executor.execute(s);

		}

		executor.shutdown();

	}


	public void archiveFile(List<Path> paths) {

		for(Path path : paths) {

			ArchiveService archiveFile;
			try {
				archiveFile = new VitamArchiveService();
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

		}

	}


}
